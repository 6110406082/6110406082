package App.FileManager;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;

public class MovieFileManager {
    private ArrayList<Movie> movies;
    private File movieDir;

    public MovieFileManager(File movieDir) {
        this.movieDir = movieDir;
        File movieList = new File(movieDir.getPath() + File.separator + "MovieList.csv");
        if (!movieList.exists()) {
            try {
                movieList.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create movieList file.");
            }
        }

        movies = new ArrayList<>();
        try {
            BufferedReader buffer = new BufferedReader(new FileReader(movieList));
            String line;
            while ((line = buffer.readLine()) != null && movies.size() < 6) {
                String[] info = line.split("\\|");
                String title = info[0].trim();
                String title2 = info[1].trim();
                String relDate = info[2].trim();
                String minute = info[3].trim();
                String type = info[4].trim();
                String director = info[5].trim();
                Movie movie = new Movie(title,title2,relDate,minute,type,director);
                movie.setSynopsis(info[6].trim());

//                File movieInfoDir = new File(movieDir.getPath() + File.separator + "Movies" + File.separator + title);
//                if (!movieInfoDir.exists())
//                    System.out.println(movieInfoDir.getPath());
//                    movieInfoDir.mkdir();
//                File movieShowTimeFile = new File(movieInfoDir.getPath() + File.separator + "showTime");
//                if (!movieShowTimeFile.exists()) {
//                    FileOutputStream fileOutputStream = new FileOutputStream(movieShowTimeFile);
//                    ObjectOutputStream outputStream = new ObjectOutputStream(fileOutputStream);
//                    outputStream.writeObject(movie.getMovieShowTimes());
//                    outputStream.flush();
//                    outputStream.close();
//                } else {
//                    FileInputStream fileInputStream = new FileInputStream(movieShowTimeFile);
//                    ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
//                    movie.setMovieShowTimes((ArrayList<MovieShowTime>) objectInputStream.readObject());
//                }

                movies.add(movie);

            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
        }
//        catch (ClassNotFoundException e) {
//            System.err.println("File cannot read.");
//        }
        catch (IOException e) {
            System.err.println("Read movie file error.");
        }
    }

    public ArrayList<Movie> getMovies() {
        return movies;
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public void addMovie(Movie movie, File poster, File video) {
        if (movies.size() < 6) {
            movies.add(movie);
            save();
        }

        try {
            File dir = new File("Data" + File.separator + "Movie" + File.separator + "Movies" + File.separator + movie.getTitle());
            Files.copy(poster.toPath(), new FileOutputStream(dir));
            Files.copy(video.toPath(), new FileOutputStream(dir));
        } catch (IOException e) {
            System.err.println("Cannot add file to directory.");
        }


    }

    public void save() {
        File movieList = new File(movieDir.getPath() + File.separator + "MovieList.csv");
        try {
            BufferedWriter buffer = new BufferedWriter(new FileWriter(movieList));

            for (Movie movie : movies) {
                buffer.write(movie.toString());
                buffer.newLine();

//                File showTimeFile = new File(movieDir.getPath() + File.separator + "Movies" + File.separator + movie.getTitle() + File.separator + "showTime.csv");
//                BufferedWriter buffer2 = new BufferedWriter(new FileWriter(showTimeFile));
//                for (String showTime : movie.getMovieShowTimes())


//                File showTimeFile = new File(movieDir.getPath() + File.separator + "Movies" + File.separator + movie.getTitle() + File.separator + "showTime");
//                ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(showTimeFile));
//                outputStream.writeObject(movie.getMovieShowTimes());
//                outputStream.flush();
//                outputStream.close();
            }
            buffer.flush();
            buffer.close();

        } catch (FileNotFoundException e) {
            System.err.println("Movie file not found.");
        } catch (IOException e) {
            System.err.println("Movie file cannot save.");
        }
    }
}
