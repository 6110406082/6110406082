package App.FileManager;

import App.SeatManager.LayoutTheater;
import App.SeatManager.NormalSeat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class User implements Serializable {
    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private boolean admin;
//    private HashMap<LayoutTheater ,ArrayList<NormalSeat>> bookedSeats;

    public User(String username, String password, String email, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.admin = false;
//        this.bookedSeats = new HashMap<>();
        if (username.equals("admin")) {
            this.admin = true;
        }
    }

    public boolean changePassword(String password, String newPassword) {
        if (this.password.equals(password)) {
            this.password = newPassword;
            return true;
        } return false;
    }

//    public void addBookedSeat(LayoutTheater theater, ArrayList<NormalSeat> seats) {
//        if (bookedSeats.containsKey(theater)) {
//            for (NormalSeat seat : seats) {
//                if (!bookedSeats.get(theater).contains(seat)) {
//                    bookedSeats.get(theater).add(seat);
//                }
//            }
//        } else {
//            bookedSeats.put(theater, seats);
//        }
//    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isAdmin() {
        return admin;
    }

//    public HashMap<LayoutTheater, ArrayList<NormalSeat>> getBookedSeats() {
//        return bookedSeats;
//    }

//    public void setBookedSeats(HashMap<LayoutTheater, ArrayList<NormalSeat>> bookedSeats) {
//        this.bookedSeats = bookedSeats;
//    }

    @Override
    public String toString() {
        return username + "," + password + "," + email + "," + firstName + "," + lastName ;
    }
}
