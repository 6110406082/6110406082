package App.FileManager;

import App.SeatManager.LayoutTheater;
import App.SeatManager.NormalSeat;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class UserFileManager {
    private ArrayList<User> users;
    private File userInfoDir;

    public UserFileManager(File userInfoDir) {
        this.userInfoDir = userInfoDir;
        File userList = new File(userInfoDir.getPath() + File.separator + "UserList.csv");
        if (!userList.exists()) {
            try {
                userList.createNewFile();
            } catch (IOException e) {
                System.err.println("Cannot create new userList.csv");
            }
        }
        users = new ArrayList<>();
        try {
            BufferedReader buffer = new BufferedReader(new FileReader(userList));
            String line;
            while ((line = buffer.readLine()) != null) {
                String[] info = line.split(",");
                String username = info[0].trim();
                String password = info[1].trim();
                String email = info[2].trim();
                String name = info[3].trim();
                String last = info[4].trim();
                User user = new User(username, password, email, name, last);

//                File usersDir = new File(userInfoDir.getPath() + File.separator + "Users");
//                if (!usersDir.exists())
//                    usersDir.mkdir();
//
//                File bookedFile = new File(usersDir.getPath() + File.separator + username);
//                if (!bookedFile.exists()) {
//                    ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(bookedFile));
//                    outputStream.writeObject(user.getBookedSeats());
//                    outputStream.flush();
//                    outputStream.close();
//                } else {
//                    ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(bookedFile));
//                    user.setBookedSeats((HashMap<LayoutTheater, ArrayList<NormalSeat>>) inputStream.readObject());
//                }
                users.add(user);
            }
            buffer.close();
        } catch (FileNotFoundException e) {
            System.err.println("User file not found.");
        }
//        catch (ClassNotFoundException e) {
//            System.err.println("User bookedFile cannot read.");
//        }
        catch (IOException e) {
            System.err.println("User file cannot read.");
        }
    }

    public boolean hasUser(String username) {
        for (User user : users) {
            if (user.getUsername().equals(username))
                return true;
        }
        return false;
    }

    public User getUser(String username) {
        for (User user : users) {
            if (user.getUsername().equals(username))
                return user;
        } return null;
    }

    public void register(User user) {
        if (!hasUser(user.getUsername())) {
            users.add(user);
        } save();
    }

    public void save() {
        File userList = new File(userInfoDir.getPath() + File.separator + "UserList.csv");
        BufferedWriter buffer = null;
        try {
            buffer = new BufferedWriter(new FileWriter(userList));

            for (User user : users) {
                buffer.write(user.toString());
                buffer.newLine();

//                File bookedFile = new File(userInfoDir.getPath() + File.separator + "Users" + File.separator + user.getUsername());
//                ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(bookedFile));
//                outputStream.writeObject(user.getBookedSeats());
//                outputStream.flush();
//                outputStream.close();

            }
            buffer.flush();
            buffer.close();

        } catch (FileNotFoundException e) {
            System.err.println("User file not found.");
        } catch (IOException e) {
            System.err.println("User file cannot save.");
            e.printStackTrace();
            try {
                assert buffer != null;
                buffer.close();
            } catch (IOException ex) {
                System.err.println("close file error.");
            }
        }
    }

}
