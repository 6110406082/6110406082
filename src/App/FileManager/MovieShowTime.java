package App.FileManager;

import App.SeatManager.LayoutTheater;
import App.SeatManager.MixLayout;
import App.SeatManager.NormalLayout;

import java.io.Serializable;
import java.util.HashMap;

public class MovieShowTime implements Serializable {
    private final LayoutTheater layout;
    private HashMap<String, LayoutTheater> showTimes;

    public MovieShowTime(LayoutTheater layout) {
        this.showTimes = new HashMap<>();
        this.layout = layout;
    }

    public void addShowTime(String time) {
        if (layout instanceof NormalLayout) {
            showTimes.put(time, new NormalLayout(layout.getSystem()));
        } else if (layout instanceof MixLayout) {
            showTimes.put(time, new MixLayout(layout.getSystem()));
        }
    }

    public LayoutTheater getLayout() {
        return layout;
    }

    public HashMap<String, LayoutTheater> getShowTimes() {
        return showTimes;
    }
}
