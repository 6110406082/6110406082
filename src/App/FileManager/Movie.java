package App.FileManager;

import java.io.File;
import java.util.ArrayList;

public class Movie {
    private String title;
    private String title2;
    private String minute;
    private String type;
    private String urlImage;
    private String urlVideo;
    private String director;
    private String relDate;
    private String synopsis;
    private ArrayList<MovieShowTime> movieShowTimes;

    public Movie(String title,String title2,String relDate, String minute,
                 String type, String director) {
        this.title = title;
        this.title2 = title2;
        this.relDate = relDate;
        this.minute = minute;
        this.type = type;
        this.urlImage = "Data" + File.separator + "Movie" + File.separator +
                "Movies" + File.separator + title + File.separator +"poster.jpg";
        this.urlVideo = "Data" + File.separator + "Movie" + File.separator +
                "Movies" + File.separator + title + File.separator + "trailer.mp4";
        this.director = director;
        this.synopsis = "";
        this.movieShowTimes = new ArrayList<>();
    }

    public void addMovieShowTime(MovieShowTime showTime) {
        if (movieShowTimes.size() < 4)
            movieShowTimes.add(showTime);
    }

    public String getTitle() {
        return title;
    }

    public String getTitle2() {
        return title2;
    }

    public String getMinute() {
        return minute;
    }

    public String getType() {
        return type;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public String getDirector() {
        return director;
    }

    public String getRelDate() {
        return relDate;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public ArrayList<MovieShowTime> getMovieShowTimes() {
        return movieShowTimes;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public void setMovieShowTimes(ArrayList<MovieShowTime> movieShowTimes) {
        this.movieShowTimes = movieShowTimes;
    }

    @Override
    public String toString() {
        return title+"|"+title2+"|"+relDate+"|"+minute+"|"+type+"|"+director+"|"+synopsis;
    }
}
