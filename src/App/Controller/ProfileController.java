package App.Controller;

import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;

public class ProfileController {
    @FXML private Button backBtn;
    @FXML private Pane pane;
    @FXML private StackPane stackPane;
    @FXML private ImageView headLogo,proFileImage;

    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                headLogo.setImage(new Image(String.valueOf(getClass().getResource("/Image/star.gif"))));
                proFileImage.setImage(new Image(String.valueOf(getClass().getResource("/Image/profileImage.jpg"))));
                setBackBtn();
            }
        });
    }

    public void setBackBtn() {
        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("Login.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();

                    root.translateYProperty().set(scene.getHeight() * -1);
                    stackPane.getChildren().add(root);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        backBtn.setOnMouseEntered(event -> backBtn.setTextFill(Color.WHITE));
        backBtn.setOnMouseExited(event -> backBtn.setTextFill(Color.BLACK));
    }
}
