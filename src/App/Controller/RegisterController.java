package App.Controller;

import App.FileManager.User;
import App.FileManager.UserFileManager;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterController {
    private UserFileManager userManager;

    @FXML private ImageView headLogo;
    @FXML private TextField usernameField,emailField,fistNameField,lastNameField;
    @FXML private PasswordField passwordField,confirmField;
    @FXML private Button backBtn,submitBtn;
    @FXML private Text errMessage,errStar1,errStar2,errStar3,errStar4,errStar5,errStar6;
    @FXML private Pane pane;
    @FXML private StackPane stackPane;

    public void initialize(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {

                errMessage.setText("");
                File userInfo = new File("Data" + File.separator + "UserInfo");
                userManager = new UserFileManager(userInfo);

                headLogo.setImage(new Image(String.valueOf(getClass().getResource("/Image/star.gif"))));


                setSubmitBtn();
                setBackBtn();
            }
        });
    }

    public void disableAllErrStar() {
        errStar1.setVisible(false);
        errStar2.setVisible(false);
        errStar3.setVisible(false);
        errStar4.setVisible(false);
        errStar5.setVisible(false);
        errStar6.setVisible(false);
    }

    public boolean containSpecialChar(String input,String pattern) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(input);
        return m.find();
    }

    public void handleSubmitBtn(Event event) {
        disableAllErrStar();
        errMessage.setText("");
        if (usernameField.getText().isEmpty()) {
            errStar1.setVisible(true);
        } if (passwordField.getText().isEmpty()) {
            errStar2.setVisible(true);
        } if (confirmField.getText().isEmpty()) {
            errStar3.setVisible(true);
        } if (emailField.getText().isEmpty()) {
            errStar4.setVisible(true);
        } if (fistNameField.getText().isEmpty()) {
            errStar5.setVisible(true);
        } if (lastNameField.getText().isEmpty()) {
            errStar6.setVisible(true);
        }
        if (!usernameField.getText().isEmpty() && !passwordField.getText().isEmpty() && !confirmField.getText().isEmpty()
        && !emailField.getText().isEmpty() && !fistNameField.getText().isEmpty() && !lastNameField.getText().isEmpty()) {
            if (containSpecialChar(usernameField.getText(), "[^A-Za-z0-9]"))
                errMessage.setText("cannot use special character");
            else if (usernameField.getText().length() < 4)
                errMessage.setText("please enter username at least 4 character");
            else if (userManager.hasUser(usernameField.getText()))
                errMessage.setText("this username is already taken");
            else if (passwordField.getText().length() < 4)
                errMessage.setText("please enter password at least 4 character");
            else if (emailField.getText().split("@").length != 2)
                errMessage.setText("email is incorrect");
            else if (!passwordField.getText().equals(confirmField.getText()))
                errMessage.setText("password is not match");
            else if (containSpecialChar(fistNameField.getText(), "[^A-Za-z]"))
                errMessage.setText("first name is in correct");
            else if (containSpecialChar(lastNameField.getText(), "[^A-Za-z0-9]"))
                errMessage.setText("last name is in correct");
            else {
                User userRegister = new User(usernameField.getText(), passwordField.getText(), emailField.getText(), fistNameField.getText(), lastNameField.getText());
                userManager.register(userRegister);
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("Login.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();

                    root.translateYProperty().set(scene.getHeight() * -1);
                    stackPane.getChildren().add(root);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        passwordField.clear();
        errStar2.setVisible(true);
        confirmField.clear();
        errStar3.setVisible(true);
    }

    public void setSubmitBtn() {
        submitBtn.setOnAction(this::handleSubmitBtn);
        submitBtn.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER))
                handleSubmitBtn(event);
        });
        submitBtn.setOnMouseEntered(event -> submitBtn.setTextFill(Color.WHITE));
        submitBtn.setOnMouseExited(event -> submitBtn.setTextFill(Color.BLACK));

        usernameField.setOnKeyPressed(submitBtn.getOnKeyPressed());
        passwordField.setOnKeyPressed(submitBtn.getOnKeyPressed());
        confirmField.setOnKeyPressed(submitBtn.getOnKeyPressed());
        emailField.setOnKeyPressed(submitBtn.getOnKeyPressed());
        fistNameField.setOnKeyPressed(submitBtn.getOnKeyPressed());
        lastNameField.setOnKeyPressed(submitBtn.getOnKeyPressed());

        confirmField.setOnKeyPressed(event -> {
            if (!confirmField.getText().equals(passwordField.getText())) {
                errMessage.setText("password is not match");
            } else {
                errMessage.setText("");
            }
        });
        passwordField.setOnKeyPressed(confirmField.getOnKeyPressed());
    }

    public void setBackBtn() {
        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("Login.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();

                    root.translateYProperty().set(scene.getHeight() * -1);
                    stackPane.getChildren().add(root);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        backBtn.setOnMouseEntered(event -> backBtn.setTextFill(Color.WHITE));
        backBtn.setOnMouseExited(event -> backBtn.setTextFill(Color.BLACK));
    }
}
