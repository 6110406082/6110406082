package App.Controller;

import App.FileManager.Movie;
import App.FileManager.User;
import App.SeatManager.LayoutTheater;
import App.SeatManager.NormalSeat;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.*;
import java.util.ArrayList;

public class TheaterController {
    private User user;
    private DropShadow dRs = new DropShadow();
    private Movie movie;
    private String timeShow;
    private LayoutTheater layoutTheater;
    private ArrayList<String> bookedSeat;
    private Button back;

    @FXML private Pane pane;
    @FXML private StackPane stackPane;
    @FXML private ImageView xWing,logo,dStar,seatPreview1,seatPreview2,seatPreview3;
    @FXML private MediaView trailer;
    @FXML private Button backBtn,submitBtn;
    @FXML private Text theaterText,seatTextPreview1,seatTextPreview2,seatTextPreview3,screen,username;

    public void setUser(User user) {
        this.user = user;
    }

    public Pane getPane() {
        return pane;
    }

    public ImageView getSeatPreview1() {
        return seatPreview1;
    }

    public ImageView getSeatPreview2() {
        return seatPreview2;
    }

    public ImageView getSeatPreview3() {
        return seatPreview3;
    }

    public Text getSeatTextPreview1() {
        return seatTextPreview1;
    }

    public Text getSeatTextPreview2() {
        return seatTextPreview2;
    }

    public Text getSeatTextPreview3() {
        return seatTextPreview3;
    }

    public Text getScreen() {
        return screen;
    }

    public ArrayList<String> getBookedSeat() {
        return bookedSeat;
    }

    public void initialize() {
        Platform.runLater(() -> {
            bookedSeat = new ArrayList<>();

            File bookingFile = new File("Data" + File.separator + "Movie" + File.separator + "BookingData.csv");
            if (!bookingFile.exists()) {
                try {
                    bookingFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else {
                FileReader fileReader = null;
                try {
                    fileReader = new FileReader(bookingFile);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                BufferedReader buffer = new BufferedReader(fileReader);

                String line = null;
                while (true) {
                    try {
                        if ((line = buffer.readLine()) == null) break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String[] bookingShowTime = line.split(",");
                    if (bookingShowTime[1].equals(movie.getTitle()) && bookingShowTime[2].equals(layoutTheater.getSystem())
                            && bookingShowTime[3].equals(timeShow)) {
                        for (int i=4;i<bookingShowTime.length;i++) {
                            if (!bookedSeat.contains(bookingShowTime[i]))
                                bookedSeat.add(bookingShowTime[i]);
                        }
                    }

                }

                back = new Button("back to homepage");
                back.setFont(Font.font("Star Jedi",13));
                back.setStyle(backBtn.getStyle());
                back.setLayoutX(19);
                back.setLayoutY(677);
                back.setEffect(new DropShadow());
                back.setOnMouseEntered(event -> back.setTextFill(Color.WHITE));
                back.setOnMouseExited(event -> back.setTextFill(Color.BLACK));
                back.setOnAction(event -> {
                    trailer.getMediaPlayer().pause();
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("Homepage.fxml"));
                        Parent root = loader.load();
                        Scene scene = pane.getScene();
                        HomePageController homePageController = loader.getController();
                        homePageController.setUser(user);

                        pane.translateYProperty().set(0);
                        stackPane.getChildren().clear();
                        stackPane.getChildren().add(root);
                        stackPane.getChildren().add(pane);

                        Timeline timeline2 = new Timeline();
                        KeyValue kv2 = new KeyValue(pane.translateYProperty(), scene.getHeight()*-1, Interpolator.EASE_OUT);
                        KeyFrame kf2 = new KeyFrame(Duration.seconds(0.7), kv2);
                        timeline2.getKeyFrames().add(kf2);
                        timeline2.setOnFinished(t -> {
                            stackPane.getChildren().remove(pane);
                        });
                        timeline2.play();

                    } catch (IOException e) {e.printStackTrace();}
                });
            }

            ((Stage) pane.getScene().getWindow()).setTitle(movie.getTitle() + " " + timeShow);
            DropShadow drS = new DropShadow();
            drS.setSpread(0.5);drS.setRadius(14.5);drS.setWidth(30);drS.setHeight(30);
            drS.setColor(new Color(0.5,0.45,0,1));
            theaterText.setEffect(drS);
            setDec();
            dRs.setSpread(0.5);dRs.setRadius(14.5);dRs.setWidth(30);dRs.setHeight(30);
            dRs.setColor(Color.WHITE);
            seatPreview1.setVisible(false);
            seatPreview2.setVisible(false);
            seatPreview3.setVisible(false);
            seatTextPreview1.setVisible(false);
            seatTextPreview2.setVisible(false);
            seatTextPreview3.setVisible(false);

            setTrailer();
            layoutTheater.setSeatLayout(this, user);
            setBackBtn();
            username.setText(user.getUsername().toLowerCase());
            xWing.toFront();

            submitBtn.setOnMouseEntered(event -> submitBtn.setTextFill(Color.WHITE));
            submitBtn.setOnMouseExited(event -> submitBtn.setTextFill(Color.BLACK));
        });
    }

    void setMovie(Movie movie) {
        this.movie = movie;
    }
    void setTimeShow(String timeShow) {
        this.timeShow = timeShow;
    }
    void setLayoutTheater(LayoutTheater layoutTheater) { this.layoutTheater = layoutTheater; }
    void setDec() {
        logo.setImage(new Image(String.valueOf(getClass().getResource("/Image/wookieLogo-pre.png"))));
        dStar.setImage(new Image(String.valueOf(getClass().getResource("/Image/deathStar.png"))));
        xWing.setImage(new Image(String.valueOf(getClass().getResource("/Image/xwing.png"))));
    }


    void setTrailer() {
        Media media = new Media(new File(movie.getUrlVideo()).toURI().toString());
        MediaPlayer player = new MediaPlayer(media);
        player.setVolume(0.5);
        player.setAutoPlay(true);
        player.setCycleCount(MediaPlayer.INDEFINITE);
        trailer.setMediaPlayer(player);
        trailer.setEffect(dRs);
        player.play();
        trailer.setOnMouseClicked(event -> {
            if (player.getStatus().equals(MediaPlayer.Status.PLAYING)) {
                player.pause();
            } else {
                player.play();
            }
        });
    }
    void setBackBtn() {
        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                trailer.getMediaPlayer().pause();
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("ShowTime.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();
                    ShowTimeController showTimeController = loader.getController();
                    showTimeController.setMovie(movie);
                    showTimeController.setUser(user);

                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(), scene.getHeight()*-1, Interpolator.EASE_OUT);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();

                } catch (IOException e) {e.printStackTrace();}
            }
        });
        backBtn.setOnMouseEntered(event -> backBtn.setStyle("-fx-background-color: #8a7828"));
        backBtn.setOnMouseExited(event -> backBtn.setStyle("-fx-background-color: #c9ac40"));
    }

    @FXML public void submit() {
        if (layoutTheater.getSelectedSeat().size() > 0) {
            StringBuilder showTimeInfo = new StringBuilder();
            for (NormalSeat s : layoutTheater.getSelectedSeat()) {
                showTimeInfo.append(",").append(s.getSeatCode());
            }
            String bookingInfo = user.getUsername()+","+movie.getTitle() + "," + layoutTheater.getSystem() + "," + timeShow + showTimeInfo.toString();

            File bookingFile = new File("Data" + File.separator + "Movie" + File.separator + "BookingData.csv");
            FileWriter fileWriter = null;
            try {
                fileWriter = new FileWriter(bookingFile,true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            BufferedWriter buffer = new BufferedWriter(fileWriter);

            try {
                buffer.write(bookingInfo);
                buffer.newLine();

                buffer.flush();
                buffer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Pane paneGround = new Pane();
            paneGround.setPrefSize(pane.getPrefWidth(),pane.getPrefHeight());
            paneGround.setStyle(pane.getStyle());
            paneGround.setOpacity(0.75);
            pane.getChildren().add(paneGround);

            Pane paneDialog = new Pane();
            paneDialog.setPrefSize(200,pane.getPrefHeight());
            paneDialog.setStyle("-fx-background-color :  #25273C");
            pane.getChildren().add(paneDialog);
            paneDialog.setTranslateX(450);

            Timeline timeline = new Timeline();
            KeyValue kv = new KeyValue(paneDialog.translateXProperty(), 250, Interpolator.EASE_IN);
            KeyFrame kf = new KeyFrame(Duration.seconds(0.5), kv);
            timeline.getKeyFrames().add(kf);
            timeline.setOnFinished(t -> {

            });
            timeline.play();

            paneDialog.getChildren().add(back);

            Text[] billText = new Text[layoutTheater.getSelectedSeat().size() + 3];
            for (int i=0;i<billText.length;i++) {
                billText[i] = new Text();
                billText[i].setEffect(new DropShadow());
                paneDialog.getChildren().add(billText[i]);
                billText[i].setFont(Font.font("Star Jedi", 13));
                billText[i].setFill(Color.web("#c9ac40"));
                billText[i].setLayoutX(33);
                billText[i].setLayoutY(130 + i * 30);
                if (i == 0) {
                    billText[0].setText("user : " + user.getUsername());
                    billText[0].setFont(Font.font("Star Jedi", 15));
                    billText[i].setLayoutX(20);
                    billText[0].setLayoutY(125);
                } else if (i == 1) {
                    billText[1].setText("seats booked.");
                } else if (i == billText.length-1) {
                    billText[i].setLayoutY(135 + i * 30);
                    billText[i].setText("total price \n" + layoutTheater.getTotalPrice() + " baht.");
                }else {
                    billText[i].setText(layoutTheater.getSelectedSeat().get(i-2).getSeatCode() +"\t\t" +layoutTheater.getSelectedSeat().get(i-2).getPrice() + "-");
                }

            }

//        if (layoutTheater.getSelectedSeats().size() > 0) {
//            File dataMovie = new File("Data" + File.separator + "Movie");
//            MovieFileManager movieFileManager = new MovieFileManager(dataMovie);
//            movieFileManager.save();
////            user.addBookedSeat(layoutTheater, layoutTheater.getSelectedSeats());
////            File dataUser = new File("Data" + File.separator + "UserInfo");
////            UserFileManager userFileManager = new UserFileManager(dataUser);
////            userFileManager.getUser(user.getUsername()).addBookedSeat(layoutTheater, layoutTheater.getSelectedSeats());
////            for (NormalSeat u : userFileManager.getUser(user.getUsername()).getBookedSeats().get(layoutTheater)) {
////                System.out.print(u.getSeatCode() + " ");
////            }
////            System.out.println();
////            userFileManager.save();
//
//
//
//            System.out.println("save");
//
//
//        }
        }
    }
}
