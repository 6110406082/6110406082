package App.Controller;

import App.FileManager.UserFileManager;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;

public class LoginController {
    private UserFileManager userManager;

    @FXML private Text errMessage;
    @FXML private ImageView deathStarLogo,headLogo,troop;
    @FXML private TextField usernameField;
    @FXML private PasswordField passwordField;
    @FXML private Button loginBtn,registerBtn,creditBtn;
    @FXML private Pane pane;
    @FXML private StackPane stackPane;

    public void initialize() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                File userInfo = new File("Data" + File.separator + "UserInfo");
                userManager = new UserFileManager(userInfo);

                errMessage.setVisible(false);
                headLogo.setImage(new Image(String.valueOf(getClass().getResource("/Image/star.gif"))));
                deathStarLogo.setImage(new Image(String.valueOf(getClass().getResource("/Image/deathStarLogo.png"))));
                troop.setImage(new Image(String.valueOf(getClass().getResource("/Image/troop.png"))));

                setLoginBtn();
                setRegisterBtn();
                setCreditBtn();
            }
        });
    }

    public void handleLogin(Event event) {
        errMessage.setVisible(false);
        if (usernameField.getText().isEmpty()) {
            errMessage.setText("please enter username");
            errMessage.setVisible(true);
            passwordField.clear();
        } else if (passwordField.getText().isEmpty()) {
            errMessage.setText("please enter password");
            errMessage.setVisible(true);
        } else {
            if (userManager.hasUser(usernameField.getText())) {
                if (userManager.getUser(usernameField.getText()).getPassword().equals(passwordField.getText())) {
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("HomePage.fxml"));
                        Parent root = loader.load();
                        HomePageController homePageController = loader.getController();
                        homePageController.setUser(userManager.getUser(usernameField.getText()));
                        Scene scene = pane.getScene();

                        root.translateYProperty().set(scene.getHeight() * -1);
                        stackPane.getChildren().add(root);

                        Timeline timeline = new Timeline();
                        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
                        KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                        timeline.getKeyFrames().add(kf);
                        timeline.setOnFinished(t -> {
                            stackPane.getChildren().remove(pane);
                        });
                        timeline.play();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    errMessage.setText("password is incorrect");
                    errMessage.setVisible(true);
                    passwordField.clear();
                }
            } else {
                errMessage.setText("user not found");
                errMessage.setVisible(true);
                passwordField.clear();
            }
        }
    }

    public void setLoginBtn() {
        loginBtn.setOnAction(this::handleLogin);
        loginBtn.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ENTER))
                handleLogin(event);
        });
        loginBtn.setOnMouseEntered(event -> loginBtn.setTextFill(Color.WHITE));
        loginBtn.setOnMouseExited(event -> loginBtn.setTextFill(Color.BLACK));

        usernameField.setOnKeyPressed(loginBtn.getOnKeyPressed());
        passwordField.setOnKeyPressed(loginBtn.getOnKeyPressed());
    }

    public void setRegisterBtn() {
        registerBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("Register.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();

                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(), scene.getHeight()*-1, Interpolator.EASE_OUT);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                } catch (IOException e) {e.printStackTrace();}
            }
        });
        registerBtn.setOnMouseEntered(event -> registerBtn.setTextFill(Color.WHITE));
        registerBtn.setOnMouseExited(event -> registerBtn.setTextFill(Color.BLACK));
    }

    public void setCreditBtn() {
        creditBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("Profile.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();

                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(), scene.getHeight()*-1, Interpolator.EASE_OUT);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                } catch (IOException e) {e.printStackTrace();}
            }
        });
        creditBtn.setOnMouseEntered(event -> creditBtn.setTextFill(Color.WHITE));
        creditBtn.setOnMouseExited(event -> creditBtn.setTextFill(Color.BLACK));
    }

}
