package App.Controller;

import App.FileManager.Movie;
import App.FileManager.MovieFileManager;
import App.FileManager.MovieShowTime;
import App.SeatManager.MixLayout;
import App.SeatManager.NormalLayout;
import App.FileManager.User;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.*;
import java.util.ArrayList;

public class HomePageController {
    private ArrayList<Movie> movies = new ArrayList<>();
    private MovieFileManager movieManager;
    private DropShadow drS = new DropShadow();
    private Timeline timeline = new Timeline();
    private User user;

    @FXML private Pane pane;
    @FXML private StackPane stackPane;
    @FXML private Text theaterText,username;
    @FXML private ImageView xWing,dStar,logo;
    @FXML private Button logoutBtn;

    public void initialize() {
        Platform.runLater(() -> {
            File movieInfo = new File("Data" + File.separator + "Movie");
            movieManager = new MovieFileManager(movieInfo);
//            movies = movieManager.getMovies();

            ((Stage) pane.getScene().getWindow()).setTitle("Wookie Theater");
            setImageView();
            drS.setSpread(0.5);
            drS.setRadius(14.5);
            drS.setWidth(30);
            drS.setHeight(30);
            drS.setColor(new Color(0.5, 0.45, 0, 1));
            theaterText.setEffect(drS);

            setMovies();
            movieManager.setMovies(movies);
            movieManager.save();

            setPoster();
            setWallLoop();
            setLogoutBtn();
            username.setText(user.getUsername().toLowerCase());
            xWing.toFront();

        });
    }

    private void setPoster() {
        ArrayList<Button> buyBtn = new ArrayList<>();
        ArrayList<ImageView> poster = new ArrayList<>();
        for (int i = 0; i < movies.size(); i++) {
            buyBtn.add(new Button("buy ticket"));
            buyBtn.get(i).setFont(Font.font("Star Jedi", 13));
            buyBtn.get(i).setStyle("-fx-background-color: #FBDB4B");
            buyBtn.get(i).setEffect(drS);
            buyBtn.get(i).setMinWidth(120);
            buyBtn.get(i).setMaxHeight(15);
            buyBtn.get(i).setPadding(new Insets(2));
            int finalI = i;
            buyBtn.get(i).setOnMouseEntered(event -> {
                buyBtn.get(finalI).setStyle("-fx-background-color: #b59d31; -fx-text-fill: white;");
                buyBtn.get(finalI).setPadding(new Insets(3));

            });
            buyBtn.get(i).setOnMouseExited(event -> {
                buyBtn.get(finalI).setStyle("-fx-background-color: #FBDB4B; -fx-text-fill: black");
                buyBtn.get(finalI).setPadding(new Insets(2));

            });
            File image = new File(movies.get(i).getUrlImage());
            poster.add(new ImageView(new Image(image.toURI().toString())));
            poster.get(i).setFitWidth(120);
            poster.get(i).setFitHeight(185);
            poster.get(i).setEffect(drS);
            poster.get(i).setOnMouseEntered(event -> {
                buyBtn.get(finalI).setStyle("-fx-background-color: #b59d31; -fx-text-fill: white;");
                buyBtn.get(finalI).setPadding(new Insets(3));
            });
            poster.get(finalI).setOnMouseExited(event -> {
                buyBtn.get(finalI).setStyle("-fx-background-color: #FBDB4B; -fx-text-fill: black");
                buyBtn.get(finalI).setPadding(new Insets(2));
            });
            if (i <= 2) {
                buyBtn.get(i).setLayoutX(22.5 + i * 142.5);
                buyBtn.get(i).setLayoutY(400);
                poster.get(i).setLayoutX(22.5 + i * 142.5);
                poster.get(i).setLayoutY(230);
            } else {
                buyBtn.get(i).setLayoutX(22.5 + (i - 3) * 142.5);
                buyBtn.get(i).setLayoutY(635);
                poster.get(i).setLayoutX(22.5 + (i - 3) * 142.5);
                poster.get(i).setLayoutY(465);
            }
            poster.get(i).setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    timeline.pause();
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("ShowTime.fxml"));
                        Parent root = loader.load();
                        Scene scene = pane.getScene();
                        ShowTimeController showTimeController = loader.getController();
                        showTimeController.setMovie(movies.get(finalI));
                        showTimeController.setUser(user);

                        root.translateYProperty().set(scene.getHeight() * -1);
                        stackPane.getChildren().add(root);

                        Timeline timeline = new Timeline();
                        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
                        KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                        timeline.getKeyFrames().add(kf);
                        timeline.setOnFinished(t -> {
                            stackPane.getChildren().remove(pane);
                        });
                        timeline.play();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            buyBtn.get(i).setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    timeline.pause();
                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("ShowTime.fxml"));
                        Parent root = loader.load();
                        Scene scene = pane.getScene();
                        ShowTimeController showTimeController = loader.getController();
                        showTimeController.setMovie(movies.get(finalI));
                        showTimeController.setUser(user);

                        root.translateYProperty().set(scene.getHeight() * -1);
                        stackPane.getChildren().add(root);

                        Timeline timeline = new Timeline();
                        KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
                        KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                        timeline.getKeyFrames().add(kf);
                        timeline.setOnFinished(t -> {
                            stackPane.getChildren().remove(pane);
                        });
                        timeline.play();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            pane.getChildren().add(poster.get(i));
            pane.getChildren().add(buyBtn.get(i));
        }
    }

    private void setWallLoop() {
        String[] wallLoop = {"wall1.jpg", "wall2.jpg", "wall3.jpg", "wall4.jpg", "wall5.jpg", "wall6.jpg"};
        double[] ds = {0, 1.75, 3.50, 5.25, 7, 8.75};
        ImageView f = new ImageView(new Image(String.valueOf(getClass().getResource("/Image/loopPic/wall1.jpg"))));
        f.setFitWidth(266.66);
        f.setFitHeight(133.18);
        Group loopGroup = new Group(f);
        loopGroup.setTranslateX(164);
        loopGroup.setTranslateY(50);
        loopGroup.setEffect(drS);

        timeline.setCycleCount(Animation.INDEFINITE);
        int index;
        for (index = 0; index < wallLoop.length; index++) {
            int finalIndex = index;
            EventHandler<ActionEvent> onFinished = event -> {
                ImageView loopImage = new ImageView(new Image(String.valueOf(getClass().getResource("/Image/loopPic/" + wallLoop[finalIndex]))));
                loopImage.setFitWidth(266.66);
                loopImage.setFitHeight(133.18);
                loopGroup.getChildren().setAll(loopImage);
            };
            Duration duration = Duration.seconds(ds[index]);
            KeyFrame kf = new KeyFrame(duration, onFinished, null, null);
            timeline.getKeyFrames().add(kf);
        }
        timeline.play();
        pane.getChildren().add(loopGroup);
    }

    public void setLogoutBtn() {
        logoutBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("Login.fxml"));
                    Parent root = loader.load();
                    Scene scene = pane.getScene();

                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(), scene.getHeight()*-1, Interpolator.EASE_OUT);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                } catch (IOException e) {e.printStackTrace();}
            }
        });
        logoutBtn.setOnMouseEntered(event -> logoutBtn.setStyle("-fx-background-color: #8a7828"));
        logoutBtn.setOnMouseExited(event -> logoutBtn.setStyle("-fx-background-color: #c9ac40"));
    }

    private void setMovies() {
        movies.add(new Movie("STAR WARS: Episode IV", "A NEW HOPE", "May 25, 1977",
                "121", "Action Adventure Fantasy", "George Lucas"));
        movies.get(0).setSynopsis("The seeds for the rise of the evil kingdom finally were sown in a seemingly routine place: trade disputes between the Republic and the International Trade Federation. Two Jedi, the guardians of peace and justice, were sent to negotiate disputes. But it's a trap! What their story will be like can be followed in the movie.");
        MovieShowTime movieShowTime1 = new MovieShowTime(new NormalLayout("normal"));
        movieShowTime1.addShowTime("11:30");
        movieShowTime1.addShowTime("14:00");
        movieShowTime1.addShowTime("16:30");
        movieShowTime1.addShowTime("19:00");
        MovieShowTime movieShowTime2 = new MovieShowTime(new MixLayout("4k cinema"));
        movieShowTime2.addShowTime("12:00");
        movieShowTime2.addShowTime("15:00");
        movieShowTime2.addShowTime("18:00");
        movieShowTime2.addShowTime("21:00");
        movies.get(0).addMovieShowTime(movieShowTime1);
        movies.get(0).addMovieShowTime(movieShowTime2);

        movies.add(new Movie("STAR WARS: Episode V", "THE EMPIRE STRIKES BACK", "May 21, 1980",
                "124", "Action Adventure Fantasy", "Irvin Kershner"));
        movies.get(1).setSynopsis("The bizarre commercial dispute became a full protest against the Republic. Anakin was tasked with protecting Sen. Padme Amidala, the loyal Republic and they fell in love. At the same time, Obi-Wan finds out that the Jedi Master has ordered the creation of a clone army based on the bounty hunter Jango Fett, which Obi-Wan determines as an assassin, trying to kill Padme, though he doesn't know why. He still doesn't know why the Jedi order the clone army. The movie ends with the revelation that Dooku, the insurrection, the mud, and all the puppets are controlled by the evil Sith Lord - who is the ruler!");
        MovieShowTime movieShowTime3 = new MovieShowTime(new NormalLayout("4k cinema"));
        movieShowTime3.addShowTime("13:00");
        movieShowTime3.addShowTime("16:40");
        movieShowTime3.addShowTime("20:20");
        MovieShowTime movieShowTime4 = new MovieShowTime(new MixLayout("3d cinema"));
        movieShowTime4.addShowTime("11:00");
        movieShowTime4.addShowTime("13:45");
        movieShowTime4.addShowTime("16:30");
        movieShowTime4.addShowTime("19:15");
        MovieShowTime movieShowTime5 = new MovieShowTime(new NormalLayout("normal"));
        movieShowTime5.addShowTime("12:30");
        movieShowTime5.addShowTime("15:00");
        movieShowTime5.addShowTime("17:30");
        movieShowTime5.addShowTime("20:00");
        movies.get(1).addMovieShowTime(movieShowTime3);
        movies.get(1).addMovieShowTime(movieShowTime4);
        movies.get(1).addMovieShowTime(movieShowTime5);

        movies.add(new Movie("STAR WARS: Episode VI", "RETURN OF THE JEDI", "May 25, 1983",
                "132", "Action Adventure Fantasy", "Richard Marquand"));
        movies.get(2).setSynopsis("Luke and Leia head to Tatooine to protect Han. But Leia is enslaved and receives a metal bikini that changes the lives of every teenager. Luke infiltrates the Punjab's palace and takes the final warning. He takes him Lando, Chewy and Han to the wells. Sarlacc, who intends to leave them to be digested over the years. But Luke goes to practice further and Yoda reveals that Darth Vader is really his father Meanwhile, Han and Leia try to stop the second death star. At one point, the Emperor asked Luke to kill Vader instead. The rebel forces destroyed the second death star and were very pleased. Leia tells Han that she loves him - and he knows!");
        MovieShowTime movieShowTime6 = new MovieShowTime(new NormalLayout("normal"));
        movieShowTime6.addShowTime("13:30");
        movieShowTime6.addShowTime("16:30");
        movieShowTime6.addShowTime("19:30");
        movieShowTime6.addShowTime("21:30");
        MovieShowTime movieShowTime7 = new MovieShowTime(new MixLayout("3d cinema"));
        movieShowTime7.addShowTime("14:30");
        movieShowTime7.addShowTime("17:50");
        movieShowTime7.addShowTime("21:10");
        MovieShowTime movieShowTime8 = new MovieShowTime(new NormalLayout("4k cinema"));
        movieShowTime8.addShowTime("14:00");
        movieShowTime8.addShowTime("18:20");
        movieShowTime8.addShowTime("22:40");
        MovieShowTime movieShowTime9 = new MovieShowTime(new MixLayout("normal"));
        movieShowTime9.addShowTime("12:45");
        movieShowTime9.addShowTime("15:30");
        movieShowTime9.addShowTime("18:15");
        movieShowTime9.addShowTime("21:00");
        movies.get(2).addMovieShowTime(movieShowTime6);
        movies.get(2).addMovieShowTime(movieShowTime7);
        movies.get(2).addMovieShowTime(movieShowTime8);
        movies.get(2).addMovieShowTime(movieShowTime9);

        movies.add(new Movie("STAR WARS: Episode I", "THE PHANTOM MENACE", "May 19, 1999",
                "136", "Action Adventure Fantasy", "George Lucas"));
        movies.get(3).setSynopsis("The seeds for the rise of the evil kingdom finally were sown in a seemingly routine place: trade disputes between the Republic and the International Trade Federation. Two Jedi, the guardians of peace and justice, were sent to negotiate disputes. But it's a trap! What their story will be like can be followed in the movie.");
        MovieShowTime movieShowTime10 = new MovieShowTime(new NormalLayout("3d cinema"));
        movieShowTime10.addShowTime("11:20");
        movieShowTime10.addShowTime("13:50");
        movieShowTime10.addShowTime("16:20");
        movieShowTime10.addShowTime("18:50");
        MovieShowTime movieShowTime11 = new MovieShowTime(new MixLayout("4k cinema"));
        movieShowTime11.addShowTime("14:10");
        movieShowTime11.addShowTime("17:20");
        movieShowTime11.addShowTime("20:30");
        movies.get(3).addMovieShowTime(movieShowTime10);
        movies.get(3).addMovieShowTime(movieShowTime11);

        movies.add(new Movie("STAR WARS: Episode II", "ATTACK OF THE CLONES", "May 16, 2002",
                "142", "Action Adventure Fantasy", "George Lucas"));
        movies.get(4).setSynopsis("The bizarre commercial dispute became a full protest against the Republic. Anakin was tasked with protecting Sen. Padme Amidala, the loyal Republic and they fell in love. At the same time, Obi-Wan finds out that the Jedi Master has ordered the creation of a clone army based on the bounty hunter Jango Fett, which Obi-Wan determines as an assassin, trying to kill Padme, though he doesn't know why. He still doesn't know why the Jedi order the clone army. The movie ends with the revelation that Dooku, the insurrection, the mud, and all the puppets are controlled by the evil Sith Lord - who is the ruler!");
        MovieShowTime movieShowTime12 = new MovieShowTime(new NormalLayout("3d cinema"));
        movieShowTime12.addShowTime("12:45");
        movieShowTime12.addShowTime("16:15");
        movieShowTime12.addShowTime("19:45");
        MovieShowTime movieShowTime13 = new MovieShowTime(new MixLayout("normal"));
        movieShowTime13.addShowTime("11:00");
        movieShowTime13.addShowTime("14:15");
        movieShowTime13.addShowTime("17:30");
        movieShowTime13.addShowTime("20:45");
        MovieShowTime movieShowTime14 = new MovieShowTime(new NormalLayout("4k cinema"));
        movieShowTime14.addShowTime("13:30");
        movieShowTime14.addShowTime("17:15");
        movieShowTime14.addShowTime("21:00");
        movies.get(4).addMovieShowTime(movieShowTime12);
        movies.get(4).addMovieShowTime(movieShowTime13);
        movies.get(4).addMovieShowTime(movieShowTime14);

        movies.add(new Movie("STAR WARS: Episode III", "REVENGE OF THE SITH", "May 19, 2005",
                "140", "Action Adventure Fantasy", "George Lucas"));
        movies.get(5).setSynopsis("The prime minister is captured by General Rebellion leader Grievous, but Obiwan and Anakin save his life bravely. The Chief Minister tries to find Anakin's dark side, demanding that Anakin assassinate Dooku, which he does. Anakin is dealing with his demons, including the dying Padme's vision of giving birth. The council refuses to elevate him to a teacher status, so the prime minister puts him in the Jedi Council as his (spy) agent. Finally, Anakin thinks that the chancellor is the Sith Lord. But when the Jedi try to arrest him, Ah Nakin comes to help him because he still wants to save Padme from the image of death.");
        MovieShowTime movieShowTime15 = new MovieShowTime(new NormalLayout("4k cinema"));
        movieShowTime15.addShowTime("14:15");
        movieShowTime15.addShowTime("18:00");
        movieShowTime15.addShowTime("21:45");
        MovieShowTime movieShowTime16 = new MovieShowTime(new MixLayout("normal cinema"));
        movieShowTime16.addShowTime("11:30");
        movieShowTime16.addShowTime("14:30");
        movieShowTime16.addShowTime("17:30");
        movieShowTime16.addShowTime("20:30");
        MovieShowTime movieShowTime17 = new MovieShowTime(new NormalLayout("normal"));
        movieShowTime17.addShowTime("12:45");
        movieShowTime17.addShowTime("15:45");
        movieShowTime17.addShowTime("18:45");
        movieShowTime17.addShowTime("21:45");
        MovieShowTime movieShowTime18 = new MovieShowTime(new MixLayout("3d cinema"));
        movieShowTime18.addShowTime("12:50");
        movieShowTime18.addShowTime("17:05");
        movieShowTime18.addShowTime("21:20");
        movies.get(5).addMovieShowTime(movieShowTime15);
        movies.get(5).addMovieShowTime(movieShowTime16);
        movies.get(5).addMovieShowTime(movieShowTime17);
        movies.get(5).addMovieShowTime(movieShowTime18);
    }

    private void setImageView() {
        logo.setImage(new Image(String.valueOf(getClass().getResource("/Image/wookieLogo-pre.png"))));
        dStar.setImage(new Image(String.valueOf(getClass().getResource("/Image/deathStar.png"))));
        xWing.setImage(new Image(String.valueOf(getClass().getResource("/Image/xwing.png"))));
    }

    public void setUser(User user) {
        this.user = user;
    }
}