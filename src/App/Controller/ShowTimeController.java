package App.Controller;

import App.FileManager.Movie;
import App.FileManager.User;
import App.SeatManager.LayoutTheater;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

public class ShowTimeController {
    private Movie movie;
    private User user;
    private ArrayList<ArrayList<String>> times = new ArrayList<>();
    private ArrayList<ArrayList<Button>> timeBtn = new ArrayList<>();
    private Text[] systemType;

    @FXML private Pane pane;
    @FXML private StackPane stackPane;
    @FXML private Text theaterText,title,title2,type,minute,director,relDate,systemType1,systemType2,systemType3,systemType4,synopsis,username;
    @FXML private MediaView trailer;
    @FXML private ImageView posterShow,dStar,logo,lego;
    @FXML private Button backBtn,synopsisBtn;
    @FXML private Group showTimeGroup;

    public void setMovie(Movie movie) {
        this.movie = movie;
        for (int i=0;i<movie.getMovieShowTimes().size();i++) {
            times.add(new ArrayList<>());
            for (String k : movie.getMovieShowTimes().get(i).getShowTimes().keySet()) {
                times.get(i).add(k);
            }
            times.get(i).sort(new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    String[] t1 = o1.split(":");
                    String[] t2 = o2.split(":");
                    if (Integer.parseInt(t1[0]) > Integer.parseInt(t2[0])) {
                        return 1;
                    } else if (Integer.parseInt(t1[0]) == Integer.parseInt(t2[0])) {
                        if (Integer.parseInt(t1[1]) > Integer.parseInt(t2[1])) {
                            return 1;
                        } else if (Integer.parseInt(t1[1]) == Integer.parseInt(t2[1])) {
                            return 0;
                        } return -1;
                    } return -1;
                }
            });

        }
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void initialize() {
        Platform.runLater(() -> {
            showTimeGroup.setVisible(true);
            synopsis.setVisible(false);
            synopsis.setText(movie.getSynopsis());
            systemType = new Text[]{systemType1, systemType2, systemType3, systemType4};
            for (Text text : systemType)
                text.setVisible(false);
            ((Stage) pane.getScene().getWindow()).setTitle(movie.getTitle());
            setImageView();
            DropShadow drS = new DropShadow();
            drS.setSpread(0.5);drS.setRadius(14.5);drS.setWidth(30);drS.setHeight(30);
            drS.setColor(new Color(0.5,0.45,0,1));
            theaterText.setEffect(drS);
            File image = new File(movie.getUrlImage());
            posterShow.setImage(new Image(image.toURI().toString()));

            setStrTime();
            setDetail();
            setTrailer();
            setBackBtn();
            username.setText(user.getUsername().toLowerCase());
            synopsisBtn.setOnMouseEntered(event -> synopsisBtn.setTextFill(Color.WHITE));
            synopsisBtn.setOnMouseExited(event -> synopsisBtn.setTextFill(Color.BLACK));
        });
    }
    private void setImageView() {
        logo.setImage(new Image(String.valueOf(getClass().getResource("/Image/wookieLogo-pre.png"))));
        dStar.setImage(new Image(String.valueOf(getClass().getResource("/Image/deathStar.png"))));
        lego.setImage(new Image(String.valueOf(getClass().getResource("/Image/trooper.png"))));
    }

    public void setStrTime() {
        for (int i=0;i<times.size();i++) {
            timeBtn.add(new ArrayList<>());
            systemType[i].setVisible(true);
            systemType[i].setText(movie.getMovieShowTimes().get(i).getLayout().getSystem());
            if (systemType[i].getText().equals("normal")) {
                systemType[i].setFill(Color.WHITE);
            } else if (systemType[i].getText().equals("3d cinema")) {
                systemType[i].setFill(Color.web("#f5527d"));
            } else if (systemType[i].getText().equals("4k cinema")) {
                systemType[i].setFill(Color.web("#4aa8ff"));
            }
            for (int j=0;j<times.get(i).size();j++) {
                DropShadow dRs = new DropShadow();
                dRs.setOffsetX(2);dRs.setOffsetY(2);
                timeBtn.get(i).add(new Button(times.get(i).get(j)));
                timeBtn.get(i).get(j).setFont(Font.font("Star Jedi",13));
                timeBtn.get(i).get(j).setStyle("-fx-background-color: #c9ac40");
                timeBtn.get(i).get(j).setEffect(dRs);
                int finalI = i;
                int finalJ = j;
                timeBtn.get(i).get(j).setOnMouseEntered(event -> {
                    timeBtn.get(finalI).get(finalJ).setStyle("-fx-background-color: #8a7828");
                });
                timeBtn.get(i).get(j).setOnMouseExited(event -> {
                    timeBtn.get(finalI).get(finalJ).setStyle("-fx-background-color: #c9ac40");
                });
                LayoutTheater layoutTheater = movie.getMovieShowTimes().get(i).getShowTimes().get(timeBtn.get(i).get(j).getText());
                timeBtn.get(i).get(j).setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        trailer.getMediaPlayer().pause();
                        try {
                            FXMLLoader loader = new FXMLLoader(getClass().getResource("Theater.fxml"));
                            Parent root = loader.load();
                            Scene scene = pane.getScene();
                            TheaterController theater = loader.getController();
                            theater.setMovie(movie);
                            theater.setLayoutTheater(layoutTheater);
                            theater.setTimeShow(timeBtn.get(finalI).get(finalJ).getText());
                            theater.setUser(user);

                            root.translateYProperty().set(scene.getHeight()*-1);
                            stackPane.getChildren().add(root);

                            Timeline timeline = new Timeline();
                            KeyValue kv = new KeyValue(root.translateYProperty(), 0, Interpolator.EASE_IN);
                            KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                            timeline.getKeyFrames().add(kf);
                            timeline.setOnFinished(t -> {
                                stackPane.getChildren().remove(pane);
                            });
                            timeline.play();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                timeBtn.get(i).get(j).setMinSize(60,30);
                timeBtn.get(i).get(j).setMaxSize(60,30);
                timeBtn.get(i).get(j).setLayoutX(53.6 + 94.6*j);
                timeBtn.get(i).get(j).setLayoutY(435 + 65*i);
                pane.getChildren().add(timeBtn.get(i).get(j));
            }
        }
    }
    public void setDetail() {
        title.setText(movie.getTitle().toLowerCase());
        title2.setText(movie.getTitle2().toLowerCase());
        type.setText(movie.getType().toLowerCase());
        minute.setText(movie.getMinute().toLowerCase() + " min");
        director.setText(movie.getDirector().toLowerCase());
        relDate.setText(movie.getRelDate().toLowerCase());
    }
    public void setTrailer() {
        DropShadow dRS = new DropShadow();
        dRS.setSpread(0.5);dRS.setRadius(14.5);dRS.setWidth(30);dRS.setHeight(30);
        dRS.setColor(Color.WHITE);
        Media media = new Media(new File(movie.getUrlVideo()).toURI().toString());
        MediaPlayer player = new MediaPlayer(media);
        player.setVolume(0.5);
        player.setAutoPlay(true);
        player.setCycleCount(MediaPlayer.INDEFINITE);
        trailer.setMediaPlayer(player);
        trailer.setEffect(dRS);
        player.play();
        trailer.setOnMouseClicked(event -> {
            if (player.getStatus().equals(MediaPlayer.Status.PLAYING)) {
                player.pause();
            } else {
                player.play();
            }
        });
    }

    public void setBackBtn() {
        backBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                trailer.getMediaPlayer().pause();
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("HomePage.fxml"));
                    Parent root = loader.load();
                    HomePageController homePageController = loader.getController();
                    homePageController.setUser(user);
                    Scene scene = pane.getScene();

                    pane.translateYProperty().set(0);
                    stackPane.getChildren().clear();
                    stackPane.getChildren().add(root);
                    stackPane.getChildren().add(pane);

                    Timeline timeline = new Timeline();
                    KeyValue kv = new KeyValue(pane.translateYProperty(), scene.getHeight()*-1, Interpolator.EASE_OUT);
                    KeyFrame kf = new KeyFrame(Duration.seconds(0.7), kv);
                    timeline.getKeyFrames().add(kf);
                    timeline.setOnFinished(t -> {
                        stackPane.getChildren().remove(pane);
                    });
                    timeline.play();
                } catch (IOException e) {e.printStackTrace();}
            }
        });
        backBtn.setOnMouseEntered(event -> backBtn.setStyle("-fx-background-color: #8a7828"));
        backBtn.setOnMouseExited(event -> backBtn.setStyle("-fx-background-color: #c9ac40"));
    }

    @FXML public void handleSynopsisBtnOnAction(ActionEvent event) {
        synopsis.setVisible(!synopsis.isVisible());
        showTimeGroup.setVisible(!showTimeGroup.isVisible());
        for (ArrayList<Button> bLst : timeBtn)
            for (Button b : bLst)
                b.setVisible(!b.isVisible());
    }
}
