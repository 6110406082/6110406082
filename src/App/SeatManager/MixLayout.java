package App.SeatManager;

import App.Controller.TheaterController;
import App.FileManager.User;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

public class MixLayout implements LayoutTheater, Serializable {
    private String system;
    private NormalSeat[][] seats;
    private String[] seatChar = {"f","e","d","c","b","a"};
    private User userLogin;
    private ArrayList<NormalSeat> selectedSeats;

    public MixLayout(String system) {
        seats = new NormalSeat[6][10];
        this.system = system;
    }

    @Override
    public void setSeatLayout(TheaterController controller, User user) {
        this.selectedSeats = new ArrayList<>();
        this.userLogin = user;
        int row = 6;
        int column = 10;
        controller.getScreen().setText(system);
        if (system.equals("normal")) {
            controller.getScreen().setFill(Color.WHITE);
        } else if (system.equals("3d cinema")) {
            controller.getScreen().setFill(Color.web("#f5527d"));
        } else if (system.equals("4k cinema")) {
            controller.getScreen().setFill(Color.web("#4aa8ff"));
        }
        for (int i=0; i<row; i++) {
            Font fontRow = Font.font("Star Jedi", 13);
            Text seatRow1 = new Text(seatChar[i]);
            Text seatRow2 = new Text(seatChar[i]);
            seatRow1.setFont(fontRow);
            seatRow2.setFont(fontRow);
            seatRow1.setFill(Color.web("#eed26a"));
            seatRow2.setFill(Color.web("#eed26a"));
            seatRow1.setEffect(new DropShadow());
            seatRow2.setEffect(new DropShadow());
            seatRow1.setLayoutX(35);
            seatRow2.setLayoutX(405);
            if (i < 3) {
                seatRow1.setLayoutY(317 + 40 * i);
                seatRow2.setLayoutY(317 + 40 * i);
            } else if (i <= 4) {
                seatRow1.setLayoutY(337 + 40 * i);
                seatRow2.setLayoutY(337 + 40 * i);
            } else {
                seatRow1.setLayoutY(357 + 40 * i);
                seatRow2.setLayoutY(357 + 40 * i);
            }
            controller.getPane().getChildren().addAll(seatRow1, seatRow2);

            for (int j=0; j<column; j++) {
                ImageView seat;
                if (i<3) {
                    seats[i][j] = new NormalSeat(seatChar[i]+(j+1),100 + i*10);
                    seat = seats[i][j].getSeatExited();
                    seat.setLayoutY(300 + 40*i);
                } else if (i<5) {
                    seats[i][j] = new PremiumSeat(seatChar[i]+(j+1),180 + i*10);
                    seat = seats[i][j].getSeatExited();
                    seat.setLayoutY(320 + 40*i);
                } else {
                    seats[i][j] = new VipSeat(seatChar[i]+(j+1),250 + i*10);
                    seat = seats[i][j].getSeatExited();
                    seat.setLayoutY(340 + 40*i);
                }
                if (j<5) seat.setLayoutX(65 + 30*j);
                else seat.setLayoutX(85 + 30*j);

                ImageView selectedLogo = seats[i][j].getSelectedLogo();
                selectedLogo.setLayoutY(seat.getLayoutY() + 3);
                selectedLogo.setLayoutX(seat.getLayoutX() + 5);
                selectedLogo.setVisible(false);

                controller.getPane().getChildren().addAll(seat,selectedLogo);

                if (controller.getBookedSeat().contains(seats[i][j].getSeatCode())) {
                    ImageView userImage = new ImageView(new Image(String.valueOf(getClass().getResource("/Image/user.png"))));
                    userImage.setVisible(true);
                    userImage.setPreserveRatio(true);
                    userImage.setFitWidth(35);
                    userImage.setLayoutX(seat.getLayoutX() - 2.55);
                    userImage.setLayoutY(seat.getLayoutY() - 2);
                    controller.getPane().getChildren().add(userImage);
                } else {
                    int finalI = i;
                    int finalJ = j;
                    seat.setOnMouseClicked(event -> {
                        if (selectedLogo.isVisible()) {
                            selectedLogo.setVisible(false);
                            seat.setOnMouseExited(event1 -> seat.setEffect(seats[finalI][finalJ].getSeatExited().getEffect()));
                            selectedSeats.remove(seats[finalI][finalJ]);
                            controller.getBookedSeat().remove(seats[finalI][finalJ].getSeatCode());
                        } else {
                            selectedLogo.setVisible(true);
                            seat.setEffect(seats[finalI][finalJ].getSeatEntered().getEffect());
                            seat.setOnMouseExited(null);
                            selectedLogo.setOnMouseClicked(seat.getOnMouseClicked());
                            selectedSeats.add(seats[finalI][finalJ]);
                            controller.getBookedSeat().add(seats[finalI][finalJ].getSeatCode());
                        }
                    });
                    seat.setOnMouseEntered(event -> seat.setEffect(seats[finalI][finalJ].getSeatEntered().getEffect()));
                    seat.setOnMouseExited(event -> seat.setEffect(seats[finalI][finalJ].getSeatExited().getEffect()));
                }
            }
        }
        controller.getSeatTextPreview1().setVisible(true);
        controller.getSeatTextPreview2().setVisible(true);
        controller.getSeatTextPreview3().setVisible(true);

        NormalSeat normalSeat = new NormalSeat("Preview",0);
        ImageView normalPreview = normalSeat.getSeatExited();
        normalPreview.setOnMouseEntered(null);
        normalPreview.setLayoutY(controller.getSeatPreview1().getLayoutY());
        normalPreview.setLayoutX(controller.getSeatPreview1().getLayoutX());
        controller.getPane().getChildren().add(normalPreview);

        PremiumSeat premiumSeat = new PremiumSeat("Preview",0);
        ImageView premiumPreview = premiumSeat.getSeatExited();
        premiumPreview.setOnMouseEntered(null);
        premiumPreview.setLayoutY(controller.getSeatPreview2().getLayoutY());
        premiumPreview.setLayoutX(controller.getSeatPreview2().getLayoutX());
        controller.getPane().getChildren().add(premiumPreview);

        VipSeat vipSeat = new VipSeat("Preview",0);
        ImageView vipPreview = vipSeat.getSeatExited();
        vipPreview.setOnMouseEntered(null);
        vipPreview.setLayoutY(controller.getSeatPreview3().getLayoutY());
        vipPreview.setLayoutX(controller.getSeatPreview3().getLayoutX());
        controller.getPane().getChildren().add(vipPreview);
    }

    @Override
    public double getTotalPrice() {
        int total = 0;
        for (NormalSeat seat : selectedSeats)
            total += seat.getPrice();
        return total;
    }

    @Override
    public String getSystem() {
        return system;
    }

    @Override
    public ArrayList<NormalSeat> getSelectedSeat() {
        return selectedSeats;
    }
}
