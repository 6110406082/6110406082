package App.SeatManager;

import App.Controller.TheaterController;
import App.FileManager.User;

import java.util.ArrayList;

public interface LayoutTheater {
    void setSeatLayout(TheaterController controller, User user);
    String getSystem();
    double getTotalPrice();
    ArrayList<NormalSeat> getSelectedSeat();
}
