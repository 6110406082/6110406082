package App.SeatManager;

import App.FileManager.User;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.File;
import java.io.Serializable;

public class NormalSeat implements Serializable {
//    private boolean booked;
    private String seatImagePath = "/Image/seat.png";
    private String selectedLogoPath = "/Image/selectedLogo.png";
    private String seatCode;
    private User owner;
    private double price;

    public NormalSeat(String seatCode, double price) {
//        this.booked = false;
        this.price = price;
        this.owner = null;
        this.seatCode = seatCode;
    }

//    public void setBooked(User user) {
//        if (booked && owner == user) {
//            booked = false;
//            owner = null;
//        } else {
//            owner = user;
//            booked = true;
//            System.out.println("booked");
//        }
//    }

//    public boolean isBooked() {
//        return booked;
//    }

    public ImageView getSeatEntered() {
        ImageView seatImage = new ImageView(new Image(String.valueOf(getClass().getResource(seatImagePath))));
        seatImage.setPreserveRatio(true);
        seatImage.setFitWidth(30);
        ColorAdjust colorEntered = new ColorAdjust();
        colorEntered.setBrightness(-0.5);
        seatImage.setEffect(colorEntered);
        return seatImage;

    }

    public ImageView getSeatExited() {
        ImageView seatImage = new ImageView(new Image(String.valueOf(getClass().getResource(seatImagePath))));
        seatImage.setPreserveRatio(true);
        seatImage.setFitWidth(30);
        ColorAdjust colorExited = new ColorAdjust();
        colorExited.setBrightness(-0.2);
        seatImage.setEffect(colorExited);
        return seatImage;
    }

    public ImageView getSelectedLogo() {
        ImageView selectedLogo = new ImageView(new Image(String.valueOf(getClass().getResource(selectedLogoPath))));
        selectedLogo.setPreserveRatio(true);
        selectedLogo.setFitWidth(20);
        selectedLogo.toFront();
        return selectedLogo;
    }

    public String getSeatCode() {
        return seatCode;
    }

    public User getOwner() {
        return owner;
    }

    public double getPrice() {
        return price;
    }


}
