package App.SeatManager;

import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;

public class VipSeat extends NormalSeat {


    public VipSeat(String seatCode, double price) {
        super(seatCode, price);
    }

    @Override
    public ImageView getSeatExited() {
        ImageView seatExited = super.getSeatExited();
        ColorAdjust superBrightness = new ColorAdjust();
        superBrightness.setBrightness(-0.2);
        ColorAdjust adjustBrightness = new ColorAdjust();
        adjustBrightness.setBrightness(-0.100);
        ColorAdjust colorSeat = new ColorAdjust();
        colorSeat.setHue(0.66);
        colorSeat.setInput(adjustBrightness);
        superBrightness.setInput(colorSeat);
        seatExited.setEffect(superBrightness);
        return seatExited;
    }

    @Override
    public ImageView getSeatEntered() {
        ImageView seatEntered = super.getSeatEntered();
        ColorAdjust superBrightness = new ColorAdjust();
        superBrightness.setBrightness(-0.5);
        ColorAdjust adjustBrightness = new ColorAdjust();
        adjustBrightness.setBrightness(-0.100);
        ColorAdjust colorSeat = new ColorAdjust();
        colorSeat.setHue(0.66);
        colorSeat.setInput(adjustBrightness);
        superBrightness.setInput(colorSeat);
        seatEntered.setEffect(superBrightness);
        return seatEntered;
    }

    @Override
    public double getPrice() {
        return 300;
    }
}
