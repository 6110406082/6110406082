package App.SeatManager;

import App.Controller.TheaterController;
import App.FileManager.User;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

public class NormalLayout implements LayoutTheater, Serializable {
    private String system;
    private NormalSeat[][] seats;
    private String[] seatChar = {"h","g","f","e","d","c","b","a"};
    private User userLogin;
    private ArrayList<NormalSeat> selectedSeats;
    private ArrayList<NormalSeat> bookedSeats;

    public NormalLayout(String system) {
        seats = new NormalSeat[8][10];
        this.system = system;
    }

    @Override
    public void setSeatLayout(TheaterController controller, User user) {
        this.selectedSeats = new ArrayList<>();
        this.userLogin = user;
        int row = 8;
        int column = 10;
        controller.getScreen().setText(system);
        if (system.equals("normal")) {
            controller.getScreen().setFill(Color.WHITE);
            controller.getScreen().setStroke(Color.BLACK);
            controller.getScreen().setStrokeWidth(1);
        } else if (system.equals("3d cinema")) {
            controller.getScreen().setFill(Color.web("#f5527d"));
            controller.getScreen().setStrokeWidth(0);
        } else if (system.equals("4k cinema")) {
            controller.getScreen().setFill(Color.web("#4aa8ff"));
            controller.getScreen().setStrokeWidth(0);
        }
        for (int i=0; i<row; i++) {
            Font fontRow = Font.font("Star Jedi", 13);
            Text seatRow1 = new Text(seatChar[i]);
            Text seatRow2 = new Text(seatChar[i]);
            seatRow1.setFont(fontRow);
            seatRow2.setFont(fontRow);
            seatRow1.setFill(Color.web("#eed26a"));
            seatRow2.setFill(Color.web("#eed26a"));
            seatRow1.setEffect(new DropShadow());
            seatRow2.setEffect(new DropShadow());
            seatRow1.setLayoutX(35);
            seatRow2.setLayoutX(405);
            if (i < 4) {
                seatRow1.setLayoutY(327 + 30 * i);
                seatRow2.setLayoutY(327 + 30 * i);
            } else {
                seatRow1.setLayoutY(357 + 30 * i);
                seatRow2.setLayoutY(357 + 30 * i);
            }
            controller.getPane().getChildren().addAll(seatRow1, seatRow2);

            for (int j=0;j<column;j++) {
                seats[i][j] = new NormalSeat(seatChar[i]+(j+1), 100 + i*10);
                ImageView seat = seats[i][j].getSeatExited();
                ImageView selectedLogo = seats[i][j].getSelectedLogo();
                if (i<4) seat.setLayoutY(310 + 30*i);
                else seat.setLayoutY(340 + 30*i);

                if (j<5) seat.setLayoutX(65 + 30*j);
                else seat.setLayoutX(85 + 30*j);

                selectedLogo.setLayoutY(seat.getLayoutY() + 3);
                selectedLogo.setLayoutX(seat.getLayoutX() + 5);
                selectedLogo.setVisible(false);

                controller.getPane().getChildren().addAll(seat,selectedLogo);

                if (controller.getBookedSeat().contains(seats[i][j].getSeatCode())) {
                    ImageView userImage = new ImageView(new Image(String.valueOf(getClass().getResource("/Image/user.png"))));
                    userImage.setVisible(true);
                    userImage.setPreserveRatio(true);
                    userImage.setFitWidth(35);
                    userImage.setLayoutX(seat.getLayoutX() - 2.55);
                    userImage.setLayoutY(seat.getLayoutY() - 2);
                    controller.getPane().getChildren().add(userImage);
                } else {
                    int finalI = i;
                    int finalJ = j;
                    seat.setOnMouseClicked(event -> {
                        if (selectedLogo.isVisible()) {
                            selectedLogo.setVisible(false);
                            seat.setOnMouseExited(event1 -> seat.setEffect(seats[finalI][finalJ].getSeatExited().getEffect()));
                            selectedSeats.remove(seats[finalI][finalJ]);
                            controller.getBookedSeat().remove(seats[finalI][finalJ].getSeatCode());
                        } else {
                            selectedLogo.setVisible(true);
                            seat.setEffect(seats[finalI][finalJ].getSeatEntered().getEffect());
                            seat.setOnMouseExited(null);
                            selectedLogo.setOnMouseClicked(seat.getOnMouseClicked());
                            selectedSeats.add(seats[finalI][finalJ]);
                            controller.getBookedSeat().add(seats[finalI][finalJ].getSeatCode());
                        }
                    });
                    seat.setOnMouseEntered(event -> seat.setEffect(seats[finalI][finalJ].getSeatEntered().getEffect()));
                    seat.setOnMouseExited(event -> seat.setEffect(seats[finalI][finalJ].getSeatExited().getEffect()));
                }
            }
        }

        controller.getSeatTextPreview1().setVisible(true);

        NormalSeat normalSeat = new NormalSeat("Normal",0);
        ImageView previewSeat = normalSeat.getSeatExited();
        previewSeat.setOnMouseEntered(null);
        previewSeat.setLayoutY(controller.getSeatPreview1().getLayoutY());
        previewSeat.setLayoutX(controller.getSeatPreview1().getLayoutX());
        controller.getPane().getChildren().add(previewSeat);
    }

    @Override
    public double getTotalPrice() {
        int total = 0;
        for (NormalSeat seat : selectedSeats)
            total += seat.getPrice();
        return total;
    }

    @Override
    public String getSystem() {
        return system;
    }

    @Override
    public ArrayList<NormalSeat> getSelectedSeat() {
        return selectedSeats;
    }
}
